<?php

namespace App\Http\Middleware;

use Closure;

class ValidateApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $headers = $request->header();
        $user_ip = $request->ip();

        if (!isset($headers['x-access-token']) || !isset($headers['x-access-token'][0])){
            return 'Token not provided';
        }



        return $next($request);
    }
}
